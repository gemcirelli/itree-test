require 'date'

DIA = 365

def get_zodiac(day, month)
  case month

  when 12
    puts 'entro al month'
    day < 22 ? 'Sagitario' : 'Capricornio'

  when 1
    day < 20 ? 'Capricornio' : 'Acuario'

  when 2
    day < 19 ? 'Acuario' : 'Piscis '

  when 3

    day < 21 ? 'Piscis' : 'Aries '

  when 4

    day < 20 ? 'Aries' : 'Tauro'

  when 5
    day < 21 ? 'Tauro' : 'Geminis'

  when 6
    day < 21 ? 'Geminis' : 'Cancer'

  when 7
    day < 23 ? 'Cancer' : 'Leo'

  when 8
    day < 23 ? 'Leo' : 'Virgo'

  when 9
    day < 23 ? 'Virgo' : 'libra'

  when 10
    day < 23 ? 'Libra' : 'scorpio'

  when 11
    day < 22 ? 'Escorpio' : 'Sagitario'

  end
end

def lucky_zodiac(fecha_actual, birthday_date)
  fecha_actual_day = Date.parse(fecha_actual).day.to_i
  birthday_date_day = Date.parse(birthday_date).day.to_i

  fecha_actual_month = Date.parse(fecha_actual).month.to_i
  birthday_date_month = Date.parse(birthday_date).month.to_i

  fecha_actual_year = Date.parse(fecha_actual).year.to_i
  birthday_date_year = Date.parse(birthday_date).year.to_i

  zodiac = get_zodiac(birthday_date_day, birthday_date_month)

  date_diff_current = Date.new(2022, fecha_actual_month, fecha_actual_day)
  date_diff_birthday = Date.new(2022, birthday_date_month, birthday_date_day)

  days_diff = date_diff_birthday.mjd - date_diff_current.mjd

  days_total = 365 - days_diff

  days_total -= 365 unless days_total < 365
  #   if days_total  > 365
  #     days_total = days_total
  # else

  #   end

  # hago drop para obtener los ultimos dos digitos del año
  lucky_number = birthday_date_year.digits.reverse.drop(2).sum
  # check para cumplir con que el numero de la suerte no sea mayor a 9
  lucky_number -= 9 if lucky_number > 9

  {
    'dias' => days_total,
    'signo' => zodiac,
    'meses' => (birthday_date_month - fecha_actual_month).abs,
    'años' => fecha_actual_year - birthday_date_year,
    'numero suerte' => lucky_number
  }
end

puts 'introduzca la fecha actual dd/mm/yyyy'
fecha_actual = gets.chomp.to_s

puts 'introduzca la fecha de cumpleaños dd/mm/yyyy'

birthday_date = gets.chomp.to_s

result = lucky_zodiac fecha_actual, birthday_date

puts " El usuario tiene #{result['años']} años con  #{result['meses']} meses #{result['dias']}  dias es de signo #{result['signo']} y su numero de la suerte es #{result['numero suerte']} "
