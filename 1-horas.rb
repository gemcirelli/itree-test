puts 'ingrese la cantidad de horas trabajadas'

horas = gets.chomp.to_i
puts 'ingrese el pago por horas'

pago = gets.chomp.to_i

if horas < 48
  total = horas * pago
else
  horas_doble = horas - 48
  pago_doble = pago + pago * 0.5
  total = (48 * pago) + (horas_doble * pago_doble)
end

puts "el total a pagar es de #{total}"
