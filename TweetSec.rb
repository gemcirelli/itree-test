require_relative 'PasswordEvaluator'
class PasswordSec
  attr_reader :user

  def initialize(user)
    @user = user
  end

  def strength_evaluator(password)
    strength = PasswordEvaluator.new password
    password_category = strength.score
    puts password_category
    if password_category >= 50
      puts "Congratulations #{user} you password has an score of #{password_category} so it is STRONG"

    elsif password_category > 10
      strengthened_password = modified_password password
      puts "Your password #{password} is WEAK I suggets you to use  #{strengthened_password} as a better option "
    else
      puts 'Unacceptable password please try with a better password'
    end
  end

  def modified_password(password)
    # metodo para mejorar el password ingresado por el usuario
    password_downcased = password.downcase
    new_password = ''

    password_downcased.each_char do |_character|
      new_password << case _character
                      when 'a' then '4'
                      when 'e' then '3'
                      when 'i' then '1'
                      when 'o' then '0'
                      else _character
                      end
    end
    new_password
  end
end

test_password = PasswordSec.new('Gabriel')

puts test_password.strength_evaluator(' goat m4n')
puts test_password.strength_evaluator(' p4ssw0rd1 34445 @j12hh')
