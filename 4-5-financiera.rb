def calcular_probabilidad(edad, estado_civil, cantidad_hijos, _fecha_contratacion, tiempo_a_calcular)
  probabilidad_desempleo = 0.00
  razones = []
  probabilidad_desempleo += 0.50 if cantidad_hijos > 10

  if estado_civil == 'casado'
    probabilidad_desempleo += 0.15 if tiempo_a_calcular >= 3
    # razones.store("casado", "es casado y será contratado por más de 3 meses ")
    razones << 'es casado y será contratado por más de 3 meses '
  elsif tiempo_a_calcular >= 6

    probabilidad_desempleo += 0.10
    razones << 'es soltero y será contratado por más de 6 meses'
  end

  if edad > 35
    if tiempo_a_calcular.between?(6, 11)
      probabilidad_desempleo += 0.05
      razones << 'tiene más de 35 años y será contratado por más de 6 meses y menos de 1 año'
    elsif tiempo_a_calcular > 11
      probabilidad_desempleo += 0.20
      razones << 'tiene más de 35 años y será contratado por más de 1 año'
    end
  elsif tiempo_a_calcular >= 6
    probabilidad_desempleo += 0.30
    razones << 'Es menor de 35 años y será contratado por más de 6 meses'
  end

  {
    'probabilidad_desempleo' => probabilidad_desempleo * 100,
    'razones' => razones
  }
end

# LLamada al metodo para realizar los calculos
decision = calcular_probabilidad(20, 'soltero', 4, '01 / 01 / 2019', 6)
# puts "la decision es #{decision['probabilidad_desempleo']}"
razon = decision['razones'].join(', ')
decision = decision['probabilidad_desempleo'].to_i

case decision
when 20
  puts "El cliente debe ser aprobado #{razon}"
when 20..40
  puts "El cliente debe ser revisado manualmente ya que #{razon} su prob de quedar desempleado es de  #{decision}%"
else
  puts "El cliente debe ser rechazado ya que #{razon}"
end
