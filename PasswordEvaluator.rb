require_relative 'parserPassword'
class PasswordEvaluator
  # incluyo mixin para transformar y evaluar la contrasena
  include ParserPassword
  attr_reader :password, :new_password, :score

  def initialize(password)
    # transformo pass y hago uso del mixin
    @password = password
    @new_password = password.downcase

    evaluation = ParserPassword.evaluate @new_password
    @score = evaluation['character_types'] * evaluation['ocurrences']
  end
end
