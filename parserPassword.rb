# mixin para evaluar la password para ver ocurrencias con los distintos tipos que existen
module ParserPassword
  class EvaluationHelper
    attr_reader :character_types, :ocurrences

    def initialize(password)
      @character_types = 0
      @ocurrences = 0
      @password = password

      #   regex for alphabet
      count_types(/[A-Za-z]+/)
      #   regex for digits
      count_types(/[0-9]+/)
      #   regex for whitespaces
      count_types(/\s+/)
      #   regex for other (punctuation,unicode,etc)
      count_types(/[^A-Za-z0-9\s]+/)
    end

    def count_types(regexp)
      #   scan me da un array con  ocurriencias que haya encontrado de la regexp match solo dev la primera
      types = @password.scan regexp
      #   puts " types son #{types}"
      if types.size > 0
        # character_types tipos de regex y ocurrences cuantas hay dentro del password
        @character_types += 1

        @ocurrences += types.size
        # puts @ocurrences
      end
    end
  end

  def self.evaluate(password)
    return 0 if password.to_s.empty?

    # if password.to_s.empty?
    #   return {
    #     'character_types' => 0,

    #   }
    # end

    evaluation = EvaluationHelper.new password
    {
      'character_types' => evaluation.character_types,
      'ocurrences' => evaluation.ocurrences
    }
  end
end
